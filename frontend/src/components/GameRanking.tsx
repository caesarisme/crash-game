import { StackProps, Table, Tbody, Td, Th, Thead, Tr } from "@chakra-ui/react";
import Section from "./ui/Section";
import { MdLeaderboard } from "react-icons/md";
import { Player } from "@/features/crashGame/crashGame.interface";
import { useMemo } from "react";

interface GameRankingProps {
  players?: Player[];
  styledProps?: StackProps;
}

const GameRanking: React.FC<GameRankingProps> = ({
  players = [],
  styledProps,
}) => {
  const sortedPlayers = useMemo(() => {
    return players.slice().sort((a, b) => b.score - a.score);
  }, [players]);

  return (
    <Section title="Ranking" icon={MdLeaderboard} styledProps={styledProps}>
      <Table size="sm">
        <Thead>
          <Tr>
            <Th>No.</Th>
            <Th>Name</Th>
            <Th isNumeric>Score</Th>
          </Tr>
        </Thead>

        <Tbody>
          {sortedPlayers.map((p, idx) => (
            <Tr key={p.id}>
              <Td>{idx + 1}</Td>
              <Td>{p.userName}</Td>
              <Td isNumeric>{p.score}</Td>
            </Tr>
          ))}
        </Tbody>
      </Table>
    </Section>
  );
};

export default GameRanking;
