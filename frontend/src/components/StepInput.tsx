import { Button, Text, Input, Stack } from "@chakra-ui/react";
import { useEffect } from "react";

interface StepInputProps {
  label?: string;
  value: number;
  onChange: (newValue: number) => any;

  minValue?: number;
  maxValue?: number;
  stepSize?: number;
  fractionDigits?: number;
  isInvalid?: boolean;
}

const StepInput: React.FC<StepInputProps> = ({
  label,
  value,
  onChange,
  minValue,
  maxValue,
  stepSize = 1,
  fractionDigits = 0,
  isInvalid,
}) => {
  const increment = () => {
    if (maxValue === undefined || value + stepSize <= maxValue) {
      onChange(value + stepSize);
    } else {
      onChange(maxValue);
    }
  };

  const decrement = () => {
    if (minValue === undefined || value - stepSize >= minValue) {
      onChange(value - stepSize);
    } else {
      onChange(minValue);
    }
  };

  // format value for decimal numbers
  const fixedValue = value.toFixed(fractionDigits);
  const change = (e: React.ChangeEvent<HTMLInputElement>) => {
    onChange(Number(e.target.value));
  };

  const blur = (e: React.ChangeEvent<HTMLInputElement>) => {
    const newValue = Number(e.target.value);

    if (maxValue !== undefined && newValue > maxValue) {
      onChange(maxValue);
    } else if (minValue !== undefined && newValue < minValue) {
      onChange(minValue);
    } else {
      onChange(newValue);
    }
  };

  return (
    <Stack
      width="200px"
      paddingX="6px"
      paddingY="2px"
      border="2px solid"
      borderColor="gray.300"
      borderRadius="4px"
      spacing="0"
      align="center"
    >
      <Text fontSize="12px">{label}</Text>

      <Stack direction="row">
        <Button data-testid="decrement-button" onClick={decrement}>
          -
        </Button>

        <Input
          data-testid="number-input"
          value={fixedValue}
          onChange={change}
          onBlur={blur}
          isInvalid={isInvalid}
        />

        <Button data-testid="increment-button" onClick={increment}>
          +
        </Button>
      </Stack>
    </Stack>
  );
};

export default StepInput;
