import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";

import Section from "./Section";

describe("<Section />", () => {
  it("Should render title", () => {
    render(<Section title="some title" />);

    expect(screen.getByText(/some title/i)).toBeInTheDocument();
  });

  it("Should render children", () => {
    render(
      <Section title="some title">
        <h1>some child</h1>
      </Section>
    );

    expect(screen.getByText(/some child/i)).toBeInTheDocument();
  });
});
