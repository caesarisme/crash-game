import { Icon, Stack, Text, As, StackProps } from "@chakra-ui/react";

interface SectionProps {
  icon?: As;
  title: string;
  children?: React.ReactNode;
  styledProps?: StackProps;
}

const Section: React.FC<SectionProps> = ({
  icon,
  title,
  children,
  styledProps,
}) => {
  return (
    <Stack gap="4px" {...styledProps}>
      <Stack fontSize="20px" direction="row" align="center">
        {icon && <Icon as={icon} />}

        <Text>{title}</Text>
      </Stack>

      {children}
    </Stack>
  );
};

export default Section;
