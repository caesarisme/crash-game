import { Player } from "@/features/crashGame/crashGame.interface";
import { Center, Flex, Stack, Text } from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { FaMedal, FaRegClock, FaUser } from "react-icons/fa";

const getTime = () => {
  const now = new Date();
  const minutes = `${now.getMinutes()}`.padStart(2, "0");
  const hours = `${now.getHours()}`.padStart(2, "0");
  return `${hours}:${minutes}`;
};

interface GeneralInfoProps {
  localPlayer?: Player;
}

const GeneralInfo: React.FC<GeneralInfoProps> = ({ localPlayer }) => {
  const [time, setTime] = useState(getTime());

  useEffect(() => {
    const timeUpdateInterval = setInterval(() => {
      setTime(getTime());
    }, 1000);

    return () => {
      clearInterval(timeUpdateInterval);
    };
  }, []);

  const leadership = (
    <Flex
      padding="6px"
      width="200px"
      border="2px solid"
      borderColor="gray.300"
      borderRadius="4px"
    >
      <Center>
        <FaMedal size="36px" />
      </Center>

      <Center flex="1">
        <Text fontSize="24px">{localPlayer?.score}</Text>
      </Center>
    </Flex>
  );

  const userName = (
    <Flex
      padding="6px"
      width="200px"
      border="2px solid"
      borderColor="gray.300"
      borderRadius="4px"
    >
      <Center>
        <FaUser size="36px" />
      </Center>

      <Center flex="1">
        <Text fontSize="24px">{localPlayer?.userName}</Text>
      </Center>
    </Flex>
  );

  const currentTime = (
    <Flex
      padding="6px"
      width="200px"
      border="2px solid"
      borderColor="gray.300"
      borderRadius="4px"
    >
      <Center>
        <FaRegClock size="36px" />
      </Center>

      <Center flex="1">
        <Text fontSize="24px">{time}</Text>
      </Center>
    </Flex>
  );

  return (
    <Stack direction="row">
      {leadership}

      {userName}

      {currentTime}
    </Stack>
  );
};

export default GeneralInfo;
