import { Box, Text } from "@chakra-ui/react";
import Chart from "chart.js/auto";
import { useState } from "react";
import { useEffect, useRef } from "react";

interface GameChartProps {
  value?: number;
  speed: number;
}

const GameChart: React.FC<GameChartProps> = ({ value = 0, speed = 1 }) => {
  const canvasRef = useRef<HTMLCanvasElement>(null);
  const chartRef = useRef<Chart>();
  const [currentValue, setCurrentValue] = useState(0);

  useEffect(() => {
    let timer: any;
    if (value) {
      let num = 0;
      const targetNum = value;
      const maxDuration = 5000 / speed;
      const maxValue = 10;
      const duration = (maxDuration * targetNum) / maxValue;
      const step = +((targetNum / duration) * 10).toFixed(2);

      const timer = setInterval(() => {
        num += step;
        if (num >= targetNum) {
          clearInterval(timer);
          num = +targetNum.toFixed(2);
        }
        setCurrentValue(+num.toFixed(2));
      }, 10);
    }

    return () => {
      clearInterval(timer);
    };
  }, [value]);

  useEffect(() => {
    let chart: Chart;

    if (canvasRef.current) {
      const data = [{ x: 0, y: 0 }];
      for (let x = 1; x <= 10; x += 0.03) {
        const y = 0.45 * x ** 2;

        data.push({ x, y });
      }

      const previousY = (ctx: any) =>
        ctx.index === 0
          ? ctx.chart.scales.y.getPixelForValue(1)
          : ctx.chart
              .getDatasetMeta(ctx.datasetIndex)
              .data[ctx.index - 1].getProps(["y"], true).y;
      const totalDuration = 5000;
      const delayBetweenPoints = totalDuration / data.length;
      const animation = {
        x: {
          type: "number",
          easing: "linear",
          duration: delayBetweenPoints,
          from: NaN, // the point is initially skipped
          delay(ctx: any) {
            if (ctx.type !== "data" || ctx.xStarted) {
              return 0;
            }
            ctx.xStarted = true;
            return ctx.index * delayBetweenPoints;
          },
        },
        y: {
          type: "number",
          easing: "linear",
          duration: delayBetweenPoints,
          from: previousY,
          delay(ctx: any) {
            if (ctx.type !== "data" || ctx.yStarted) {
              return 0;
            }
            ctx.yStarted = true;
            return ctx.index * delayBetweenPoints;
          },
          onComplete: (ctx: any) => {
            if (ctx.datasetIndex === 0 && ctx.parsed.x >= 5) {
              console.log("STOP");
              chart.stop();
            }
          },
        },
      };

      chart = new Chart(canvasRef.current, {
        type: "line",
        data: {
          labels: data.map((d) => d.x),
          datasets: [
            {
              label: "qwe",
              data: data,
              fill: false,
              tension: 0,
              borderWidth: 3,
              pointBorderColor: "transparent",
              pointBackgroundColor: "transparent",
            },
          ],
        },
        options: {
          animations: animation as any,
          plugins: {
            legend: {
              display: false,
            },
          },
          interaction: {
            intersect: false,
          },
          scales: {
            y: {
              ticks: {
                display: false,
              },
              border: {
                display: false,
              },
              grid: {
                display: false,
              },
            },
            x: {
              type: "linear",
              grid: {
                display: false,
              },
            },
          },
        },
      });

      chartRef.current = chart;
    }

    return () => {
      chart?.destroy();
    };
  }, [canvasRef]);

  return (
    <Box>
      <Text fontSize="24px">{currentValue}x</Text>
      <canvas ref={canvasRef} />
    </Box>
  );
};

export default GameChart;
