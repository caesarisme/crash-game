import { Box, Button, Input, Stack, StackProps } from "@chakra-ui/react";
import Section from "./ui/Section";
import { useState } from "react";
import { Message, Player } from "@/features/crashGame/crashGame.interface";

interface GameChatProps {
  styledProps: StackProps;
  messages: Message[];
  localPlayer?: Player;
  players?: Player[];
  onSendMessage: (body: string) => any;
}

const GameChat: React.FC<GameChatProps> = ({
  styledProps,
  messages,
  players,
  localPlayer,
  onSendMessage,
}) => {
  const [text, setText] = useState("");
  const sendMessage = () => {
    onSendMessage(text);
    setText("");
  };
  const chatInput = (
    <Stack direction="row" padding="16px">
      <Input value={text} onChange={(e) => setText(e.target.value)} />
      <Button onClick={sendMessage}>Send</Button>
    </Stack>
  );

  const getPlayerNameById = (playerId: string) => {
    if (playerId === localPlayer?.id) {
      return localPlayer.userName;
    }

    return players?.find((p) => p.id === playerId)?.userName || "Unknown";
  };

  const chatHistory = (
    <Box>
      {messages.map((m) => (
        <Box key={m.id}>
          {getPlayerNameById(m.playerId)}:{m.body}
        </Box>
      ))}
    </Box>
  );

  return (
    <Section title="Chat" {...styledProps}>
      <>
        {chatHistory}

        {chatInput}
      </>
    </Section>
  );
};

export default GameChat;
