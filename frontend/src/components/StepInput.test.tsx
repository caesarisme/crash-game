import { fireEvent, render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import StepInput from "./StepInput";
import "@testing-library/jest-dom";

describe("<StepInput />", () => {
  let change = jest.fn();

  beforeEach(() => {
    change = jest.fn();
  });

  it("Should display initial value and label", () => {
    render(<StepInput label="some label" value={5} onChange={change} />);

    const input = screen.getByTestId<HTMLInputElement>("number-input");

    expect(Number(input.value)).toEqual(5);
    expect(screen.getByText(/some label/i)).toBeInTheDocument();
  });

  it("Should display decimal value", () => {
    render(<StepInput value={5} fractionDigits={2} onChange={change} />);

    const input = screen.getByTestId<HTMLInputElement>("number-input");

    expect(input.value).toEqual("5.00");
  });

  it("Should increment on increment button click", () => {
    render(<StepInput value={5} stepSize={0.25} onChange={change} />);

    const incrementButton = screen.getByTestId("increment-button");
    fireEvent.click(incrementButton);

    expect(change).toHaveBeenLastCalledWith(5.25);
  });

  it("Should decrement on decrement button click", () => {
    render(<StepInput value={5} stepSize={0.5} onChange={change} />);

    const decrementButton = screen.getByTestId("decrement-button");
    fireEvent.click(decrementButton);

    expect(change).toHaveBeenLastCalledWith(4.5);
  });

  it("Should respect maxValue on incrementation", () => {
    render(
      <StepInput value={5} maxValue={10} stepSize={10} onChange={change} />
    );

    const incrementButton = screen.getByTestId("increment-button");
    fireEvent.click(incrementButton);

    expect(change).toHaveBeenLastCalledWith(10);
  });

  it("Should respect minValue on decrementation", () => {
    render(
      <StepInput value={5} minValue={0} stepSize={10} onChange={change} />
    );

    const decrementButton = screen.getByTestId("decrement-button");
    fireEvent.click(decrementButton);

    expect(change).toHaveBeenLastCalledWith(0);
  });
});
