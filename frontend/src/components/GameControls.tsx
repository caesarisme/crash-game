import {
  Button,
  Stack,
  Table,
  Thead,
  Tr,
  Th,
  Td,
  Tbody,
  Icon,
  Text,
  Box,
  Slider,
  SliderMark,
  SliderTrack,
  SliderFilledTrack,
  SliderThumb,
  Center,
  Input,
  BoxProps,
  StackProps,
} from "@chakra-ui/react";
import StepInput from "./StepInput";
import { useMemo, useState } from "react";
import Section from "./ui/Section";
import { FaTrophy } from "react-icons/fa";
import { SiSpeedtest } from "react-icons/si";
import {
  CrashGameBetPlaceInput,
  CrashGameBetResults,
  Player,
} from "@/features/crashGame/crashGame.interface";

interface GameControlsProps {
  players?: Player[];
  localPlayer?: Player;
  betResults?: CrashGameBetResults;
  onPlaceBet: (bet: CrashGameBetPlaceInput) => any;
  onSubmitUserName: (userName: string) => any;
  speed: number;
  onChangeSpeed: (newSpeed: number) => any;
  styledProps: BoxProps | StackProps;
}

interface PlayerDisplayInfo {
  id: string;
  userName: string;
  points: string;
  multiplier: string;
  didWin?: boolean;
}

const GameControls: React.FC<GameControlsProps> = ({
  players,
  localPlayer,
  betResults,
  speed,
  onChangeSpeed,
  onPlaceBet,
  onSubmitUserName,
  styledProps,
}) => {
  const isJoined = !!localPlayer;
  const showResults = !!betResults;

  const [userName, setUserName] = useState("");
  const [points, setPoints] = useState(100);
  const [multiplier, setMultiplier] = useState(1);

  const roundData = useMemo<PlayerDisplayInfo[]>(() => {
    if (betResults && players) {
      return players?.map((p) => ({
        id: p.id,
        userName: p.userName,
        points: betResults[p.id].points.toString(),
        multiplier: betResults[p.id].multiplier.toFixed(2),
        didWin: betResults[p.id].didWin,
      }));
    } else if (players) {
      return players?.map((p) => ({
        id: p.id,
        userName: p.userName,
        points: "-",
        multiplier: "-",
      }));
    }

    return [];
  }, [players, betResults]);

  const start = () => {
    onPlaceBet({
      points,
      multiplier,
    });
  };

  const submitUserName = () => onSubmitUserName(userName);

  const inputs = (
    <Stack>
      <Stack direction="row">
        {/* Points */}
        <StepInput
          label="Points"
          value={points}
          minValue={1}
          stepSize={10}
          onChange={setPoints}
        />

        {/* Multiplier */}
        <StepInput
          label="Multiplier"
          value={multiplier}
          onChange={setMultiplier}
          minValue={1}
          maxValue={10}
          stepSize={0.25}
          fractionDigits={2}
        />
      </Stack>
      <Button onClick={start}>Start</Button>
    </Stack>
  );

  const roundPlayer = (player: PlayerDisplayInfo) => {
    const color = showResults
      ? player.didWin
        ? "green.500"
        : "red.500"
      : "black";

    return (
      <Tr key={player.id} role="group" color={color}>
        <Td _groupHover={{ bg: "gray.200" }}>{player.userName}</Td>
        <Td _groupHover={{ bg: "gray.200" }} isNumeric>
          {player.points}
        </Td>
        <Td _groupHover={{ bg: "gray.200" }} isNumeric>
          {player.multiplier}
        </Td>
      </Tr>
    );
  };

  const roundInfo = (
    <Section title="Current Round" icon={FaTrophy}>
      <Table size="sm" colorScheme="gray" variant="striped">
        <Thead>
          <Tr>
            <Th>Name</Th>
            <Th isNumeric>Points</Th>
            <Th isNumeric>Multiplier</Th>
          </Tr>
        </Thead>
        <Tbody>{roundData.map(roundPlayer)}</Tbody>
      </Table>
    </Section>
  );

  const speedControl = (
    <Stack gap="4px">
      <Stack fontSize="20px" direction="row" align="center">
        <Icon as={SiSpeedtest} />
        <Text>Speed</Text>
      </Stack>

      <Box
        padding="12px"
        border="2px solid"
        borderColor="gray.300"
        borderRadius="4px"
      >
        <Slider
          id="slider"
          defaultValue={1}
          min={1}
          max={5}
          value={speed}
          colorScheme="teal"
          onChange={onChangeSpeed}
        >
          <SliderMark value={1} mt="1" fontSize="sm">
            1x
          </SliderMark>
          <SliderMark value={2} mt="1" ml="-2.5" fontSize="sm">
            2x
          </SliderMark>
          <SliderMark value={3} mt="1" ml="-2.5" fontSize="sm">
            3x
          </SliderMark>
          <SliderMark value={4} mt="1" ml="-2.5" fontSize="sm">
            4x
          </SliderMark>
          <SliderMark value={5} mt="1" ml="-2.5" fontSize="sm">
            5x
          </SliderMark>

          <SliderTrack>
            <SliderFilledTrack />
          </SliderTrack>

          <SliderThumb backgroundColor="teal.500" />
        </Slider>
      </Box>
    </Stack>
  );

  if (!isJoined) {
    return (
      <Center
        height="500px"
        borderRadius="4px"
        backgroundColor="gray.100"
        padding="32px"
        {...styledProps}
      >
        <Stack width="100%" gap="48px">
          <Text textAlign="center" fontSize="24px">
            Welcome
          </Text>

          <Stack width="100%">
            <Text fontSize="14px" textAlign="center">
              Please Insert Your Name
            </Text>
            <Input
              value={userName}
              onChange={(e) => setUserName(e.target.value)}
              backgroundColor="white"
            />
            <Button colorScheme="green" onClick={submitUserName}>
              Accept
            </Button>
          </Stack>
        </Stack>
      </Center>
    );
  }

  return (
    <Stack gap="16px" {...styledProps}>
      {inputs}

      {roundInfo}

      {speedControl}
    </Stack>
  );
};

export default GameControls;
