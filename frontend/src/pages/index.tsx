import { Center, Flex, Stack, Text } from "@chakra-ui/react";
import Head from "next/head";
import { useEffect, useRef, useState } from "react";

import GameChart from "@/components/GameChart";
import GameRanking from "@/components/GameRanking";
import GameChat from "@/components/GameChat";
import GameControls from "@/components/GameControls";
import {
  Ack,
  AckStatus,
  CrashGameClientEvent,
  CrashGameServerEvent,
  CrashGameBetPlaceInput,
  Message,
  MessageCreateInput,
  CrashGameRound,
  CrashGameRoundStatus,
  CrashGameRoomOutput,
  Player,
  ProfileUpdateInput,
} from "@/features/crashGame/crashGame.interface";
import io, { Socket } from "socket.io-client";
import GeneralInfo from "@/components/GeneralInfo";

export default function Home() {
  // state
  const socketRef = useRef<Socket>();
  const [messages, setMessages] = useState<Message[]>([]);
  const [localPlayer, setLocalPrayer] = useState<Player>();
  const [room, setRoom] = useState<CrashGameRoomOutput>();
  const [animationSpeed, setAnimationSpeed] = useState(1);
  const [roundResults, setRoundResults] = useState<
    (CrashGameRound & { status: CrashGameRoundStatus.Finished }) | null
  >();

  // actions
  const sendMessage = (body: string) => {
    const newMessage: MessageCreateInput = {
      body,
    };
    socketRef.current?.emit(
      CrashGameClientEvent.MessageSend,
      newMessage,
      (ack: Ack<Message>) => {
        if (ack.status === AckStatus.Ok) {
          onNewMessage(ack.data);
        }
      }
    );
  };
  const placeBet = (bet: CrashGameBetPlaceInput) => {
    setRoundResults(null);
    socketRef.current?.emit(CrashGameClientEvent.PlaceBet, bet);
  };
  const updateProfile = (userName: string) => {
    const input: ProfileUpdateInput = {
      userName: userName,
    };
    socketRef.current?.emit(
      CrashGameClientEvent.ProfileUpdate,
      input,
      (ack: Ack<Player>) => {
        if (ack.status === AckStatus.Ok) {
          setLocalPrayer(ack.data);
        }
      }
    );
  };
  const joinRoom = () => {
    socketRef.current?.emit(
      CrashGameClientEvent.RoomJoin,
      (ack: Ack<CrashGameRoomOutput>) => {
        if (ack.status === AckStatus.Ok) {
          console.log(ack);
          setRoom(ack.data);
        }
      }
    );
  };
  const setNameAndJoinRoom = (userName: string) => {
    updateProfile(userName);
    joinRoom();
  };

  // handlers
  const onNewMessage = (message: Message) => {
    setMessages((prev) => [...prev, message]);
  };
  const onRoundResults = (
    round: CrashGameRound & { status: CrashGameRoundStatus.Finished }
  ) => {
    setRoundResults(round);
  };

  useEffect(() => {
    const socket = io("http://localhost:4000");
    socket.on(CrashGameServerEvent.MessageNew, onNewMessage);
    socket.on(CrashGameServerEvent.RoundResults, onRoundResults);
    socket.on(CrashGameServerEvent.RoomUpdate, setRoom);
    socket.on(CrashGameServerEvent.ProfileUpdate, setLocalPrayer);

    socketRef.current = socket;

    return () => {
      socket.disconnect();
    };
  }, []);

  return (
    <>
      <Head>
        <title>Crash game</title>
        <meta name="description" content="Crash game" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Center paddingTop="200px">
        <Stack as="main" paddingX="auto" width="1000px" spacing="24px">
          <Flex gap="16px">
            <GameControls
              players={room?.players}
              localPlayer={localPlayer}
              betResults={roundResults?.betResults}
              onPlaceBet={placeBet}
              onSubmitUserName={setNameAndJoinRoom}
              speed={animationSpeed}
              onChangeSpeed={setAnimationSpeed}
              styledProps={{ width: "400px" }}
            />

            <Stack flex="1" gap="16px">
              <GeneralInfo localPlayer={localPlayer} />

              <GameChart
                value={roundResults?.multiplier}
                speed={animationSpeed}
              />
            </Stack>
          </Flex>

          <Flex gap="16px">
            <GameRanking
              players={room?.players}
              styledProps={{ flexGrow: 1 }}
            />

            <GameChat
              styledProps={{ flexGrow: 1 }}
              messages={messages}
              localPlayer={localPlayer}
              players={room?.players}
              onSendMessage={sendMessage}
            />
          </Flex>
        </Stack>
      </Center>
    </>
  );
}
