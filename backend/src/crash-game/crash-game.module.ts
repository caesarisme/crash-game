import { Module } from '@nestjs/common';
import { CrashGameGateway } from './crash-game.gateway';

@Module({
  providers: [CrashGameGateway],
})
export class CrashGameModule { }
