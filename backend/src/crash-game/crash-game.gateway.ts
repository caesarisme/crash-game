import {
  ConnectedSocket,
  MessageBody,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import * as uuid from 'uuid';
import {
  CrashGameBet,
  CrashGameBetPlaceInput,
  CrashGameClientEvent,
  CrashGameRoom,
  CrashGameRoundStatus,
  CrashGameServerEvent,
  Message,
  MessageCreateInput,
  Ack,
  AckStatus,
  CrashGameBetResults,
  CrashGameRound,
  CrashGameRoomOutput,
  Player,
  PlayersObj,
  ProfileUpdateInput,
} from './crash-game.interface';

@WebSocketGateway({ cors: true })
export class CrashGameGateway {
  @WebSocketServer() server: Server;

  public connectedPlayers: PlayersObj = {};
  public room: CrashGameRoom = {
    players: [],
    messages: [],
    currentRound: {
      bets: [],
      status: CrashGameRoundStatus.Idle,
    },
  };

  constructor() {
    this.addBotsToRoom();
  }

  handleConnection(@ConnectedSocket() socket: Socket) {
    console.log('UserConnected', socket.id);
    const newPlayer = {
      id: socket.id,
      userName: 'Test',
      score: 1000,
      isBot: false,
    };

    this.connectedPlayers[socket.id] = newPlayer;
  }

  handleDisconnect(@ConnectedSocket() socket: Socket) {
    console.log('UserDisconnected', socket.id);

    const playerId = socket.id;
    this.room.players = this.room.players.filter((p) => p.id !== playerId);
    delete this.connectedPlayers[playerId];
  }

  @SubscribeMessage(CrashGameClientEvent.ProfileUpdate)
  handleProfileUpdate(
    @ConnectedSocket() socket: Socket,
    @MessageBody() data: ProfileUpdateInput,
  ): Ack<Player> {
    console.log('UserProfileChange', data);

    const playerId = socket.id;
    const connectedPlayer = this.connectedPlayers[playerId];
    if (connectedPlayer) {
      connectedPlayer.userName = data.userName;
    }

    const roomPlayer = this.room.players.find((p) => p.id === playerId);
    if (roomPlayer) {
      roomPlayer.userName = data.userName;
    }

    socket.broadcast.emit(CrashGameServerEvent.RoomUpdate, this.room);

    return {
      status: AckStatus.Ok,
      data: this.connectedPlayers[playerId],
    };
  }

  @SubscribeMessage(CrashGameClientEvent.RoomJoin)
  handleJoinRoom(@ConnectedSocket() socket: Socket): Ack<CrashGameRoomOutput> {
    console.log('UserJoinRoom', socket.id);
    const playerId = socket.id;
    const player = this.connectedPlayers[playerId];
    this.room.players.push(player);

    const { currentRound, ...roomInfo } = this.room;

    return {
      status: AckStatus.Ok,
      data: roomInfo,
    };
  }

  @SubscribeMessage(CrashGameClientEvent.RoomInfo)
  handleRoomInfo(): Ack<CrashGameRoomOutput> {
    const { currentRound, ...roomInfo } = this.room;

    return {
      status: AckStatus.Ok,
      data: roomInfo,
    };
  }

  @SubscribeMessage(CrashGameClientEvent.MessageSend)
  handleNewMessage(
    @ConnectedSocket() socket: Socket,
    @MessageBody() data: MessageCreateInput,
  ): Ack<Message> {
    console.log('UserMessage', socket.id);

    const newMessage: Message = {
      id: new Date().toISOString(),
      body: data.body,
      playerId: socket.id,
      createdAt: new Date().toISOString(),
    };
    this.room.messages.push(newMessage);

    socket.broadcast.emit(CrashGameServerEvent.MessageNew, newMessage);

    return {
      status: AckStatus.Ok,
      data: newMessage,
    };
  }

  @SubscribeMessage(CrashGameClientEvent.PlaceBet)
  handleNewBet(
    @ConnectedSocket() socket: Socket,
    @MessageBody() data: CrashGameBetPlaceInput,
  ): Ack<CrashGameBet> {
    const currentPlayer = this.room.players.find((p) => p.id === socket.id);
    const existingBet = this.room.currentRound.bets.find(
      (b) => b.playerId === socket.id,
    );
    const isValidBet =
      data.multiplier >= 1 && data.points <= currentPlayer.score;

    if (!existingBet && isValidBet) {
      const newBet: CrashGameBet = {
        id: new Date().toISOString(),
        points: data.points,
        multiplier: data.multiplier,
        playerId: socket.id,
      };
      this.room.currentRound.bets.push(newBet);

      this.createBotBets();
      this.startRound();

      return {
        status: AckStatus.Ok,
        data: newBet,
      };
    }

    return {
      status: AckStatus.Error,
    };
  }

  startRound() {
    if (!this.isRoundReady()) return;

    const currentRoundMultiplier = parseFloat(
      (Math.random() * 9 + 1).toFixed(2),
    );

    const betResults: CrashGameBetResults = {};

    this.room.players.forEach((player) => {
      const playerBet = this.room.currentRound.bets.find(
        (b) => b.playerId === player.id,
      );
      const didWin = playerBet.multiplier <= currentRoundMultiplier;

      if (didWin) {
        const betFinalPoints = playerBet.points * playerBet.multiplier;
        player.score = player.score + betFinalPoints - playerBet.points;

        betResults[player.id] = {
          id: playerBet.id,
          points: betFinalPoints,
          multiplier: playerBet.multiplier,
          didWin: true,
        };
      } else {
        player.score = player.score - playerBet.points;

        betResults[player.id] = {
          id: playerBet.id,
          points: 0,
          multiplier: playerBet.multiplier,
          didWin: false,
        };
      }

      if (!player.isBot) {
        const connectedPlayer = this.connectedPlayers[player.id];
        connectedPlayer.score = player.score;

        this.server.emit(CrashGameServerEvent.ProfileUpdate, player);
      }
    });

    const roundResults: CrashGameRound = {
      ...this.room.currentRound,
      betResults,
      status: CrashGameRoundStatus.Finished,
      multiplier: currentRoundMultiplier,
    };
    this.resetCurrentRound();

    this.server.emit(CrashGameServerEvent.RoomUpdate, this.room);
    this.server.emit(CrashGameServerEvent.RoundResults, roundResults);
  }

  resetCurrentRound() {
    this.room.currentRound = {
      bets: [],
      status: CrashGameRoundStatus.Idle,
    };
  }

  addBotsToRoom() {
    const nBots = 4;

    for (let i = 0; i < nBots; i++) {
      this.room.players.push({
        id: `bot${i + 1}`,
        userName: `CPU ${i + 1}`,
        score: 1000,
        isBot: true,
      });
    }
  }

  createBotBets() {
    this.room.players
      .filter((p) => p.isBot)
      .forEach((bot) => {
        const randomMultiplier = parseFloat((Math.random() * 9 + 1).toFixed(2));

        this.room.currentRound.bets.push({
          id: uuid.v4(),
          playerId: bot.id,
          points: 100,
          multiplier: randomMultiplier,
        });
      });

    console.log(this.room.currentRound.bets);
  }

  isRoundReady() {
    if (this.room.currentRound.bets.length === this.room.players.length) {
      return true;
    }

    return false;
  }
}
