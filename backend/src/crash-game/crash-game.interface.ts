export const CrashGameServerEvent = {
  MessageNew: 'message:new',
  RoundResults: 'round:results',
  RoomUpdate: 'room:update',
  ProfileUpdate: 'profile:update',
};

export const CrashGameClientEvent = {
  MessageSend: 'message:new',
  PlaceBet: 'bet:new',
  RoomInfo: 'room:info',
  RoomJoin: 'room:join',
  ProfileUpdate: 'profile:update',
};

export interface Player {
  id: string;
  userName: string;
  score: number;
  isBot: boolean;
}

export type ProfileUpdateInput = Pick<Player, 'userName'>;

export type PlayersObj = {
  [playerId: string]: Player;
};

export type WaitingPlayers = {
  [playerId: string]: Player;
};

export interface Message {
  id: string;
  body: string;
  playerId: string;
  createdAt: string;
}

export type MessageCreateInput = Pick<Message, 'body'>;

export interface CrashGameBet {
  id: string;
  points: number;
  multiplier: number;
  playerId: string;
}

export type CrashGameBetPlaceInput = Pick<
  CrashGameBet,
  'points' | 'multiplier'
>;

export type CrashGameBetResult = Pick<
  CrashGameBet,
  Exclude<keyof CrashGameBet, 'playerId'>
> & { didWin: boolean };

export type CrashGameBetResults = {
  [playerId: string]: CrashGameBetResult;
};

export enum CrashGameRoundStatus {
  Idle = 'idle',
  Finished = 'finished',
}

export type CrashGameRound =
  | {
    bets: Array<CrashGameBet>;
    status: CrashGameRoundStatus.Idle;
  }
  | {
    bets: Array<CrashGameBet>;
    multiplier: number;
    betResults: CrashGameBetResults;
    status: CrashGameRoundStatus.Finished;
  };

export interface CrashGameRoom {
  players: Player[];
  messages: Message[];
  currentRound: CrashGameRound;
}

export type CrashGameRoomOutput = Pick<CrashGameRoom, 'players' | 'messages'>;

export enum AckStatus {
  Ok = 'ok',
  Error = 'error',
}

interface AckWithoutData {
  status: AckStatus.Error;
}

interface AckWithData<T> {
  status: AckStatus.Ok;
  data: T;
}

export type Ack<T> = AckWithData<T> | AckWithoutData;
