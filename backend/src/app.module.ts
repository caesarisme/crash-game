import { Module } from '@nestjs/common';
import { CrashGameModule } from './crash-game/crash-game.module';

@Module({
  imports: [CrashGameModule],
})
export class AppModule { }
