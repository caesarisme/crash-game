import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { io, Socket } from 'socket.io-client';
import {
  Ack,
  AckStatus,
  CrashGameBet,
  CrashGameBetPlaceInput,
  CrashGameClientEvent,
  CrashGameRoomOutput,
} from '../src/crash-game/crash-game.interface';
import { CrashGameGateway } from '../src/crash-game/crash-game.gateway';

const getClientConnection = (app: INestApplication): Socket => {
  const httpServer = app.getHttpServer();

  if (!httpServer.address()) {
    httpServer.listen(0);
  }

  return io(`http://localhost:${httpServer.address().port}`, {
    autoConnect: false,
  });
};

const createNestApp = async (): Promise<INestApplication> => {
  const moduleFixture: TestingModule = await Test.createTestingModule({
    providers: [CrashGameGateway],
  }).compile();

  const app = moduleFixture.createNestApplication();
  await app.init();

  return app;
};

describe('Crash Game', () => {
  let app: INestApplication;
  let socket: Socket;

  beforeEach(async () => {
    app = await createNestApp();
    socket = getClientConnection(app);
  });

  afterEach(() => {
    socket.disconnect();
    app.close();
  });

  it('Should add new player to connectedPlayers', () => {
    socket.connect();
    const gateway = app.get(CrashGameGateway);

    socket.on('connect', () => {
      expect(gateway.connectedPlayers).toHaveProperty(socket.id);
    });
  });

  it('Should remove player on disconnect', () => {
    socket.connect();
    const gateway = app.get(CrashGameGateway);

    socket.on('connect', () => {
      expect(gateway.connectedPlayers).toHaveProperty(socket.id);
      socket.disconnect();
    });

    socket.on('disconnect', () => {
      expect(gateway.connectedPlayers).not.toHaveProperty(socket.id);
    });
  });

  it('Should add user to room and acknowledge', () => {
    socket.connect();

    const gateway = app.get(CrashGameGateway);

    socket.on('connect', () => {
      socket.emit(
        CrashGameClientEvent.RoomJoin,
        (ack: Ack<CrashGameRoomOutput>) => {
          expect(ack.status).toEqual(AckStatus.Ok);
          expect(
            gateway.room.players.find((p) => p.id === socket.id),
          ).toBeTruthy();
        },
      );
    });
  });

  it('Should add new bet to current round and acknowledge', () => {
    socket.connect();
    const gateway = app.get(CrashGameGateway);

    const newBet: CrashGameBetPlaceInput = {
      points: 100,
      multiplier: 1,
    };

    socket.on('connect', () => {
      socket.emit(
        CrashGameClientEvent.RoomJoin,
        (ack: Ack<CrashGameRoomOutput>) => {
          expect(ack.status).toEqual(AckStatus.Ok);

          socket.emit(
            CrashGameClientEvent.PlaceBet,
            newBet,
            (betAck: Ack<CrashGameBet>) => {
              expect(betAck.status).toEqual(AckStatus.Ok);
              expect(
                gateway.room.currentRound.bets.find(
                  (b) => b.playerId === socket.id,
                ),
              ).toBeTruthy();
            },
          );
        },
      );
    });
  });
});
